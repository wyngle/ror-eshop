require 'omniauth-oauth2'

Rails.application.config.middleware.use OmniAuth::Builder do
  options = {}
  options.merge!({client_options: {site: 'http://localhost:5000'}}) if Rails.env.test? || Rails.env.development?
  provider :wyngle, ENV['WYNGLE_APP_ID'], ENV['WYNGLE_SECRET'], options
end

OmniAuth.config.logger = Rails.logger
OmniAuth.config.on_failure = Proc.new { |env|
  OmniAuth::FailureEndpoint.new(env).redirect_to_failure
}