class SessionsController < ApplicationController
  skip_before_action :authenticate_user

  # E.g. GET "/auth/failure?message=access_denied&origin=http://localhost:3001/&strategy=wyngle"
  #
  def auth_failure
    strategy = params[:strategy].titleize
    message  = params[:message].humanize
    if params[:message] == 'access_denied'
      flash[:error] = "Sorry, your access via #{strategy} has been denied"
    else
      flash[:error] = "Sorry, but #{strategy} authentication failed with error: #{message}"
    end
    redirect_to root_url
  end

  # We need to locate user and either create him/her and sign him/her in, or
  # just sign him/her in.
  #
  def auth_wyngle_callback
    Rails.logger.debug request.env.inspect
    raw_info = request.env['omniauth.auth']

    wyngle_auth = Authentication::Wyngle.new(raw_info)
    user = wyngle_auth.find_or_create_user
    if user.nil?
      reset_session
      flash[:error] = "Sorry, you can't log in. Why don't you try again?"
      redirect_to root_url
    else
      reset_session
      sign_in(user)
      flash[:success] = "Hello #{user.first_name}, #{user.last_name}!"
      redirect_to dashboard_url
    end
  end

  def sign_out
    reset_session
    flash[:success] = 'Bye!'
    redirect_to root_url
  end

  private

  # @param user {User}
  #
  def sign_in(user)
    session[:user_id] = user.id
  end
end