class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :authenticate_user

  helper_method :current_user

  private

  def authenticate_user
    user = current_user
    if user.nil?
      flash[:error] = "You need to log in before you can access this page!"
      redirect_to root_url
    end
  end

  def current_user
    User.find_by_id session[:user_id]
  end
end
