module Authentication
  class Wyngle
    attr_reader :raw_info

    # @param raw_info {OmniAuth::AuthHash} Omniauth Authentication Provider raw info
    # as filled in when +request.env['omniauth.auth']
    #
    # example:
    #
    #    raw_info.provider ... which should be +:wyngle+
    #    raw_info.uid      ... which should be the user id in the provider database
    #    raw_info.info {OmniAuth::AuthHash::InfoHash}
    #       raw_info.info['email']
    #       raw_info.info['gender']
    #       raw_info.info['first_name']
    #       raw_info.info['last_name']
    #       raw_info.info['time_zone']
    #       raw_info.info['address']['address']
    #       raw_info.info['address']['address_expanded']
    #       raw_info.info['address']['region']
    #       raw_info.info['address']['city']
    #       raw_info.info['address']['postcode']
    #       raw_info.info['address']['country']
    #
    def initialize(raw_info)
      @raw_info = raw_info
    end

    def find_or_create_user
      user = User.find_by_auth_provider_and_auth_provider_id raw_info.provider, raw_info.uid
      if user.nil?
        create_user!
      else
        update_user! user
      end
    end

    def create_user!
      User.create! email: raw_info.info['email'], auth_provider: raw_info.provider, auth_provider_id: raw_info.uid, auth_provider_data: raw_info.info
    end

    def update_user! user
      user.email = raw_info.info['email'] unless raw_info.info['email'].blank?
      user.auth_provider_data = raw_info.info
      user.save!
      user
    end
  end
end