class User < ActiveRecord::Base
  serialize :auth_provider_data, OmniAuth::AuthHash::InfoHash

  validates :auth_provider, presence: true
  validates :auth_provider_id, presence: true, uniqueness: {scope: :auth_provider, case_sensitive: false}

  def first_name
    auth_provider_data['first_name']
  end

  def last_name
    auth_provider_data['last_name']
  end

  def phone
    auth_provider_data['phone']
  end

  def gender
    auth_provider_data['gender']
  end

  def address
    auth_provider_data['address']
  end
end