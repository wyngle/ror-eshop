class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :email                           # Some providers give this, some others do not
      t.string :auth_provider,     null: false  # Takes the value 1) ror-e-shop 2) wyngle
      t.string :auth_provider_id,  null: false  # the identity of the user within providers database
      t.text   :auth_provider_data              # raw data as provided by auth provider

      t.timestamps
    end

    add_index :users, [:auth_provider, :auth_provider_id], unique: true, name: 'users_auth_provider_auth_provider_id'
  end
end
