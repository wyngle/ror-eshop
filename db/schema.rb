# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141206071950) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "users", force: true do |t|
    t.string   "email"
    t.string   "auth_provider",      null: false
    t.string   "auth_provider_id",   null: false
    t.text     "auth_provider_data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["auth_provider", "auth_provider_id"], name: "users_auth_provider_auth_provider_id", unique: true, using: :btree

end
